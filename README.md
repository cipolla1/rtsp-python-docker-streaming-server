
To run server use docker-compose:

```
docker-compose up --build
```

Then to test it as client using vlc on another terminal window run:

```
vlc -v rtsp://127.0.0.1:8554/stream1
```

If you prefer opencv as client install it on your local virtual enviroment and run

```
python client/client.py
```
